import AuthLayout from "@/components/layouts/one/components/layouts/AuthLayout"
import DefaultLayout from "@/components/layouts/one/components/layouts/DefaultLayout"

export { AuthLayout, DefaultLayout }
