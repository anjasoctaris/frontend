import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from "./store"
import vuetify from './plugins/vuetify';
import "./registerServiceWorker"
import "roboto-fontface/css/roboto/roboto-fontface.css"
import "font-awesome/css/font-awesome.css"

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	vuetify,
	el: '#app',
	render: h => h(App)
});
